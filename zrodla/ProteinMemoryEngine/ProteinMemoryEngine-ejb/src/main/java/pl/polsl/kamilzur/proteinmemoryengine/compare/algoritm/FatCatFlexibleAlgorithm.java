package pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm;

import org.biojava.nbio.structure.StructureException;
import org.biojava.nbio.structure.align.StructureAlignmentFactory;
import org.biojava.nbio.structure.align.fatcat.FatCatFlexible;
import org.biojava.nbio.structure.align.fatcat.calc.FatCatParameters;

/**
 *
 * @author Kamil Żur
 */
public class FatCatFlexibleAlgorithm extends AlgorithmImpl {

    public static final String ALGORITHM_NAME = FatCatFlexible.algorithmName;

    public static FatCatFlexibleAlgorithm getInstance() throws StructureException {
        return new FatCatFlexibleAlgorithm();
    }

    private FatCatFlexibleAlgorithm() throws StructureException {
        super(new FatCatParameters(), StructureAlignmentFactory.getAlgorithm(ALGORITHM_NAME));
    }
}
