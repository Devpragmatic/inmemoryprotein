package pl.polsl.kamilzur.proteinmemoryengine.storage;

/**
*
* @author Kamil Żur
*/
public final class ProteinStorageFactory {

    public static ProteinStorage getProteinStorage(){
            return ProteinStorageMemory.getInstance();
    }
}