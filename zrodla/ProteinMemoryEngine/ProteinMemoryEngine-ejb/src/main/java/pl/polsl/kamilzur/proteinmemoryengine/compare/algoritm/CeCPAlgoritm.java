package pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm;

import org.biojava.nbio.structure.StructureException;
import org.biojava.nbio.structure.align.StructureAlignmentFactory;
import org.biojava.nbio.structure.align.ce.CECPParameters;
import org.biojava.nbio.structure.align.ce.CeCPMain;

/**
 *
 * @author Kamil Żur
 */
public class CeCPAlgoritm extends AlgorithmImpl {

    public static final String ALGORITHM_NAME = CeCPMain.algorithmName;

    public static CeCPAlgoritm getInstance() throws StructureException {
        return new CeCPAlgoritm();
    }

    private CeCPAlgoritm() throws StructureException {
        super(new CECPParameters(), StructureAlignmentFactory.getAlgorithm(ALGORITHM_NAME));
    }
}
