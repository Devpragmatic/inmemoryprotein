package pl.polsl.kamilzur.proteinmemoryengine.beans;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.polsl.kamilzur.proteinmemoryengine.Config;
import pl.polsl.kamilzur.proteinmemoryengine.entities.Query;
import pl.polsl.kamilzur.proteinmemoryengine.entities.ServerParameter;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;
import pl.polsl.kamilzur.proteinmemoryengine.remotebeaninterfaces.ManagerServerRemote;
import pl.polsl.kamilzur.proteinmemoryengine.resource.ResourceManager;
import pl.polsl.kamilzur.proteinmemoryengine.storage.ProteinStorageFactory;
import pl.polsl.kamilzur.proteinmemoryengine.threads.Cleaner;
import pl.polsl.kamilzur.proteinmemoryengine.threads.FillProteinList;

/**
 *
 * @author Kamil Żur
 */
@Stateless
public class ManagerServerBean implements ManagerServerRemote {

    @Override
    public String startServer(Integer clientsInOneTime,
            Integer howManyElementsWithOneQuery,
            String fromInAllList,
            String toInAllList,
            List<String> customFiles) {
        try {
            Config.setClientsInOneTime(clientsInOneTime);
            Config.setHowManyElementsWithOneQuery(howManyElementsWithOneQuery);
            ProteinStorageFactory.getProteinStorage().clear();
            FillProteinList.runThread(new Query(customFiles, fromInAllList, toInAllList, null, null, null));
            return new JSONObject().put("result", new ServerParameter(clientsInOneTime,
                    howManyElementsWithOneQuery,
                    fromInAllList,
                    toInAllList,
                    customFiles).toString()).toString();
        } catch (JSONException ex) {
            Logger.getLogger(ManagerServerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "{}";
    }

    @Override
    public void stopServer() {
    }

    @Override
    public String getFilesInServer() {
        JSONObject jsonResult = new JSONObject();
        try {
            ResourceManager rm = new ResourceManager();
            List<String> filesName = rm.getFilesName(
                    rm.getAllFileFromResource("/protein", ".ent.gz"), ".ent.gz");
            JSONArray jsonFilesNames = new JSONArray();
            for (String name : filesName) {
                jsonFilesNames.put(name);
            }
            jsonResult.put("files", jsonFilesNames);

        } catch (InformationException | JSONException ex) {
            Logger.getLogger(ManagerServerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonResult.toString();
    }

    @Override
    public void startCleaner() {
        new Thread(new Cleaner()).start();
    }

}
