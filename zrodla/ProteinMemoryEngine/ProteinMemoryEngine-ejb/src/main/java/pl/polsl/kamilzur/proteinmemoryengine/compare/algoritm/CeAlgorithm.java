package pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm;

import org.biojava.nbio.structure.StructureException;
import org.biojava.nbio.structure.align.StructureAlignmentFactory;
import org.biojava.nbio.structure.align.ce.CeMain;
import org.biojava.nbio.structure.align.ce.CeParameters;

/**
 *
 * @author Kamil Żur
 */
public class CeAlgorithm extends AlgorithmImpl {

    public static final String ALGORITHM_NAME = CeMain.algorithmName;

    public static CeAlgorithm getInstance() throws StructureException {
        return new CeAlgorithm();
    }

    private CeAlgorithm() throws StructureException {
        super(new CeParameters(), StructureAlignmentFactory.getAlgorithm(ALGORITHM_NAME));
    }
}
