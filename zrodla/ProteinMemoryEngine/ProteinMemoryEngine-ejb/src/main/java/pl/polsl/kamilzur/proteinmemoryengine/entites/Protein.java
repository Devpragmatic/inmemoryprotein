package pl.polsl.kamilzur.proteinmemoryengine.entites;

import org.biojava.nbio.structure.Atom;

/**
 *
 * @author Kamil Żur
 */
public class Protein {

    private final String name;
    private final Atom[] atomsCA;

    public Protein(String name, Atom[] atomsCA) {
        this.name = name;
        this.atomsCA = atomsCA;
    }

    public String getName() {
        return name;
    }

    public Atom[] getAtoms() {
        return atomsCA;
    }

    public boolean equals(Protein obj) {
        return this.name.equals(obj.getName());
    }

}
