package pl.polsl.kamilzur.proteinmemoryengine.beans;

import javax.ejb.Stateless;
import pl.polsl.kamilzur.proteinmemoryengine.remotebeaninterfaces.LoginRemote;

/**
 *
 * @author Kamil Żur
 */
@Stateless
public class LoginBean implements LoginRemote {

    @Override
    public Boolean login(String login, String password) {
        return login.equals("admin") && password.equals("admin1");
    }

}
