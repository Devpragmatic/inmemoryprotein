package pl.polsl.kamilzur.proteinmemoryengine.storage;

import java.util.List;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;

/**
 *
 * @author Kamil Żur
 */
public interface ProteinStorage {

    void addProtein(Protein protein) throws InformationException;

    void addProteinList(List<Protein> proteins);

    Protein getProtein(String name) throws InformationException;

    void delete(String name) throws InformationException;

    void clear();

    List<Protein> getProteinList() throws InformationException;
}
