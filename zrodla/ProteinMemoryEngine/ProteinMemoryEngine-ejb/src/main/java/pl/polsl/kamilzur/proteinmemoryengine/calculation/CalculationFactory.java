package pl.polsl.kamilzur.proteinmemoryengine.calculation;

import org.biojava.nbio.structure.StructureException;

/**
 *
 * @author Kamil Żur
 */
public class CalculationFactory {

    public static Calculation getAlgorithm(String calculateName) {
        try {
            if (null != calculateName) {
                switch (calculateName) {
                    case TorsionAngleCalculation.ALGORITHM_NAME:
                        return TorsionAngleCalculation.getInstance();
                    case RMSDCalculation.ALGORITHM_NAME:
                        return RMSDCalculation.getInstance();
                }
            }

            return null;
        } catch (StructureException ex) {
            return null;
        }
    }
;

}
