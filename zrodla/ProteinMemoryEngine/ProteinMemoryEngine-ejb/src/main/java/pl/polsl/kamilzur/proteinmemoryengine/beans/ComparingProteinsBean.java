package pl.polsl.kamilzur.proteinmemoryengine.beans;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm.CeAlgorithm;
import pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm.CeCPAlgoritm;
import pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm.FatCatFlexibleAlgorithm;
import pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm.FatCatRigidAlgorithm;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Result;
import pl.polsl.kamilzur.proteinmemoryengine.entities.Query;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;
import pl.polsl.kamilzur.proteinmemoryengine.remotebeaninterfaces.ComparingProteinsRemote;
import pl.polsl.kamilzur.proteinmemoryengine.resource.ResourceManager;
import pl.polsl.kamilzur.proteinmemoryengine.threads.CompareThreat;

/**
 *
 * @author Kamil Żur
 */
@Stateless
public class ComparingProteinsBean implements ComparingProteinsRemote {

    private static final ConcurrentHashMap<Integer, List<Result>> RESULTS = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<Integer, Date> TIMER = new ConcurrentHashMap<>();
    private static final Object OBJECT_SYNCHRONIZED_RESULTS = new Object();

    @Override
    public Integer comparingProteinCE(String from,
            String to,
            List<String> names,
            String protein, String chain, Integer id) throws InformationException {
        Query query = new Query(names, from, to, protein, chain, id);
        ResourceManager rm = new ResourceManager();
        if (!rm.isChain(protein, chain)) {
            throw new InformationException("Protein " + protein + " don't have chain " + chain);
        }
        query.setId(CompareThreat.runThread(query, CeAlgorithm.ALGORITHM_NAME));
        if (query.getId() == -1) {
            throw new InformationException("Server have problem with running compare. Try later.");
        }
        return query.getId();
    }

    public static void addResult(Integer id, String result, String html) {
        synchronized (OBJECT_SYNCHRONIZED_RESULTS) {
            if (RESULTS.get(id) == null) {
                RESULTS.put(id, new ArrayList<Result>());
                TIMER.put(id, new Date());
            }
            RESULTS.get(id).add(new Result(result, html));
        }
    }

    public static void checkTimer() {
        synchronized (OBJECT_SYNCHRONIZED_RESULTS) {
            Enumeration<Integer> elements = TIMER.keys();
            List<Integer> toDelete = new ArrayList<>();
            while (elements.hasMoreElements()) {
                Integer id = elements.nextElement();
                Date date = TIMER.get(id);
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.DAY_OF_MONTH, 2);
                if (date.before(cal.getTime())) {
                    toDelete.add(id);
                }
            }
            for (Integer id : toDelete) {
                RESULTS.remove(id);
                TIMER.remove(id);
            }
        }
    }

    @Override
    public String getResultCE(Integer id) {
        JSONObject result = new JSONObject();
        try {
            result.put("id", id);
            JSONArray values = new JSONArray();
            synchronized (OBJECT_SYNCHRONIZED_RESULTS) {
                if (RESULTS.get(id) != null) {
                    for (Result str : RESULTS.get(id)) {
                        try {
                            JSONObject json = XML.toJSONObject(str.getXml());
                            json.put("html", "<pre>" + str.getHtml() + "</pre>");
                            values.put(json);
                        } catch (JSONException ex) {
                            JSONObject json = new JSONObject();
                            json.put("problem", str);
                            values.put(json);
                        }
                    }
                }
            }
            result.put("results", values);

        } catch (JSONException ex) {
            Logger.getLogger(ComparingProteinsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result.toString();
    }

    @Override
    public Integer comparingProteinCeCP(String from, String to, List<String> names, String protein, String chain, Integer id) throws InformationException {
        Query query = new Query(names, from, to, protein, chain, id);
        ResourceManager rm = new ResourceManager();
        if (!rm.isChain(protein, chain)) {
            throw new InformationException("Protein " + protein + " don't have chain " + chain);
        }
        query.setId(CompareThreat.runThread(query, CeCPAlgoritm.ALGORITHM_NAME));
        if (query.getId() == -1) {
            throw new InformationException("Server have problem with running compare. Try later.");
        }
        return query.getId();
    }

    @Override
    public Integer comparingProteinFatCatFlexible(String from, String to, List<String> names, String protein, String chain, Integer id) throws InformationException {
        Query query = new Query(names, from, to, protein, chain, id);
        ResourceManager rm = new ResourceManager();
        if (!rm.isChain(protein, chain)) {
            throw new InformationException("Protein " + protein + " don't have chain " + chain);
        }
        query.setId(CompareThreat.runThread(query, FatCatFlexibleAlgorithm.ALGORITHM_NAME));
        if (query.getId() == -1) {
            throw new InformationException("Server have problem with running compare. Try later.");
        }
        return query.getId();
    }

    @Override
    public Integer comparingProteinFatCatRigid(String from, String to, List<String> names, String protein, String chain, Integer id) throws InformationException {
        Query query = new Query(names, from, to, protein, chain, id);
        ResourceManager rm = new ResourceManager();
        if (!rm.isChain(protein, chain)) {
            throw new InformationException("Protein " + protein + " don't have chain " + chain);
        }
        query.setId(CompareThreat.runThread(query, FatCatRigidAlgorithm.ALGORITHM_NAME));
        if (query.getId() == -1) {
            throw new InformationException("Server have problem with running compare. Try later.");
        }
        return query.getId();
    }

}
