/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.kamilzur.proteinmemoryengine.threads;

import java.util.logging.Level;
import java.util.logging.Logger;
import pl.polsl.kamilzur.proteinmemoryengine.beans.CalculateProteinBean;
import pl.polsl.kamilzur.proteinmemoryengine.beans.ComparingProteinsBean;

/**
 *
 * @author Kamil Żur
 */
public class Cleaner implements Runnable {

    @Override
    public void run() {
        while (true) {
            try {
                ComparingProteinsBean.checkTimer();
                CalculateProteinBean.checkTimer();
                Thread.sleep(3600000);

            } catch (InterruptedException ex) {
                Logger.getLogger(Cleaner.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
