package pl.polsl.kamilzur.proteinmemoryengine.storage;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import pl.polsl.kamilzur.proteinmemoryengine.Config;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;

/**
 *
 * @author Kamil Żur
 */
public class ProteinStorageMemory implements ProteinStorage {

    private static final ProteinStorageMemory istance = new ProteinStorageMemory();

    public static ProteinStorage getInstance() {
        return istance;
    }

    private final List<Protein> LIST = new CopyOnWriteArrayList<>();

    @Override
    public void addProtein(Protein protein) throws InformationException {
        if (Config.getHowManyElementsWithOneQuery() != null ? Config.getHowManyElementsWithOneQuery() > LIST.size()
                : true) {
            try {
                LIST.add(protein);
            } catch (Exception e) {
                final String message = "Out of memory " + e.getMessage();
                System.out.println(message);
                throw new InformationException(message);
            }
        } else {
            throw new InformationException("Lista przepełniona");
        }

    }

    @Override
    public void addProteinList(List<Protein> proteins) {
        LIST.addAll(proteins);
    }

    @Override
    public List<Protein> getProteinList() throws InformationException {
        return LIST;
    }

    @Override
    public Protein getProtein(String name) throws InformationException {
        for (Protein protein : LIST) {
            if (protein.getName().equals(name)) {
                return protein;
            }
        }
        return null;
    }

    @Override
    public void delete(String name) throws InformationException {
        synchronized (LIST) {
            for (Protein protein : LIST) {
                if (protein.getName().equals(name)) {
                    LIST.remove(protein);
                    break;
                }
            }

        }
    }

    @Override
    public void clear() {
        LIST.clear();
    }
}
