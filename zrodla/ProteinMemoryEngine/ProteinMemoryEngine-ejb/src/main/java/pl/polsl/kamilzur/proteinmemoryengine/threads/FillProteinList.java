package pl.polsl.kamilzur.proteinmemoryengine.threads;

import java.io.File;
import java.util.List;

import pl.polsl.kamilzur.proteinmemoryengine.entities.Query;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;
import pl.polsl.kamilzur.proteinmemoryengine.resource.ResourceManager;
import pl.polsl.kamilzur.proteinmemoryengine.storage.ProteinStorageFactory;

/**
 *
 * @author Kamil Żur
 */
public class FillProteinList implements Runnable {

	private static List<File> FILES;
	private static final Object OBJECT_SYNCHRONIZED_FILES = new Object();
	private ResourceManager resourceManager;
	private Integer id;

	protected FillProteinList(Query query) {
		try {
			resourceManager = new ResourceManager();
			synchronized (OBJECT_SYNCHRONIZED_FILES) {
				FILES = resourceManager.getAllFileFromResource(query, "/protein", ".ent.gz");
			}
		} catch (InformationException ex) {

		}
	}

	public static Integer runThread(Query query) {
		FillProteinList fileProteinThread = new FillProteinList(query);
		Integer id = fileProteinThread.getId();
		Thread thread = new Thread(fileProteinThread);
		thread.start();
		return id;
	}

	public Integer getId() {
		return id;
	}

	@Override
	public void run() {
		boolean loop = true;
		while (loop) {
			File file = null;
			synchronized (OBJECT_SYNCHRONIZED_FILES) {
				if (FILES != null) {
					if (FILES.size() >= 1) {
						file = FILES.remove(0);
					} else {
						FILES.remove(id);
						loop = false;
					}

				} else {
					loop = false;
				}
			}
			if (resourceManager != null && file != null) {
				try {
					ProteinStorageFactory.getProteinStorage().addProtein(resourceManager.parseFileToProtein(file, ""));
				} catch (InformationException ex) {

					loop = false;

				}
			} else {
				loop = false;
			}
		}
	}
}
