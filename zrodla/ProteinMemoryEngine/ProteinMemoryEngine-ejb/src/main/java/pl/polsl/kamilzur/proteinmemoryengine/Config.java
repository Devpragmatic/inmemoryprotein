package pl.polsl.kamilzur.proteinmemoryengine;

import pl.polsl.kamilzur.proteinmemoryengine.entities.ServerParameter;

/**
 *
 * @author Kamil Żur
 */
public class Config {

    private static final ServerParameter SERVER_PARAMETERS = new ServerParameter();
    private static final Object SYNCHRONIZED_SERVER_PARAMETER = new Object();

    public static void setClientsInOneTime(Integer clienInOneTime) {
        synchronized (SYNCHRONIZED_SERVER_PARAMETER) {
            SERVER_PARAMETERS.setClientsInOneTime(clienInOneTime);
        }
    }

    public static Integer getClientsInOneTime() {
        synchronized (SYNCHRONIZED_SERVER_PARAMETER) {
            return SERVER_PARAMETERS.getClientsInOneTime();
        }
    }

    public static void setHowManyElementsWithOneQuery(Integer howManyElementsWithOneQuery) {
        synchronized (SYNCHRONIZED_SERVER_PARAMETER) {
            SERVER_PARAMETERS.setHowManyElementsWithOneQuery(howManyElementsWithOneQuery);
        }
    }

    public static Integer getHowManyElementsWithOneQuery() {
        synchronized (SYNCHRONIZED_SERVER_PARAMETER) {
            return SERVER_PARAMETERS.getHowManyElementsWithOneQuery();
        }
    }
}
