package pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm;

import org.biojava.nbio.structure.StructureException;
import org.biojava.nbio.structure.align.model.AFPChain;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;

/**
 *
 * @author Kamil Żur
 */
public interface Algorithm {

    public AFPChain compare(Protein proteinFirst, Protein proteinSecond) throws StructureException;
}
