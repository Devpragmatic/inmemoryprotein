package pl.polsl.kamilzur.proteinmemoryengine.resource;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.biojava.nbio.structure.Atom;
import org.biojava.nbio.structure.Structure;
import org.biojava.nbio.structure.StructureException;
import org.biojava.nbio.structure.StructureTools;
import org.biojava.nbio.structure.io.PDBFileReader;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;
import pl.polsl.kamilzur.proteinmemoryengine.entities.Query;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;

/**
 * Klasa odpowiedzialna za wczytywanie, parsowanie oraz wyszukiwanie plików.
 *
 * @author Kamil Żur
 */
public class ResourceManager {

    /**
     * Czy patrzeć w podfolderach
     */
    private boolean recursive;

    public boolean isRecursive() {
        return recursive;
    }

    public void setRecursive(boolean recursive) {
        this.recursive = recursive;
    }
    /**
     * Lista ścieżek do plików.
     */
    private final List<File> files;

    /**
     * Konstruktor inicjalizujący parametry oraz tworzący liste.
     */
    public ResourceManager() {
        files = new CopyOnWriteArrayList<>();
        recursive = true;
    }

    /**
     * Funkcja zwracająca liste plików z danym rozszerzeniem występujący w
     * folderze.
     *
     * @param path ścieżka do folderu
     * @param extension rozszerzenie pliku.
     * @return lista ścieżek do plików.
     * @throws InformationException wyrzuca wyjątek z problemami w trakcie
     * przetwarzania funkcji.
     */
    public List<File> getAllFileFromResource(String path, String extension) throws InformationException {
        files.clear();
        try {
            URL url = getClass().getResource(path);
            File directory = new File(url.getFile());
            if (!directory.isDirectory()) {
                throw new InformationException("Błędna ścieżka do folderu");
            }
            getContentOfFilesInDirectory(directory, extension);
        } catch (IOException e) {
            throw new InformationException("Problemy z plikiem");
        }
        return files;
    }

    /**
     * Funkcja zwracająca liste plików z danym rozszerzeniem występujący w
     * folderze.
     *
     * @param query
     * @param path ścieżka do folderu
     * @param extension rozszerzenie pliku.
     * @return lista ścieżek do plików.
     * @throws InformationException wyrzuca wyjątek z problemami w trakcie
     * przetwarzania funkcji.
     */
    public List<File> getAllFileFromResource(Query query, String path, String extension) throws InformationException {
        files.clear();
        try {
            URL url = getClass().getResource(path);
            File directory = new File(url.getFile());
            if (!directory.isDirectory()) {
                throw new InformationException("Błędna ścieżka do folderu");
            }
            getContentOfFilesInDirectory(query, directory, extension);
        } catch (IOException e) {
            throw new InformationException("Problemy z plikiem");
        }
        return files;
    }

    public List<String> getFilesName(List<File> files, String extension) {
        List<String> names = new ArrayList<>();
        for (File file : files) {
            if (file.getName().lastIndexOf(extension) > -1) {
                names.add(file.getName().substring(0, file.getName().lastIndexOf(extension)));
            }
        }
        return names;
    }

    /**
     * Funkcja zwracająca plik z danym o podanej nazwie białka.
     *
     * @param name nazwa białka
     * @return lista ścieżek do plików.
     */
    public File getFileFromResource(String name) {
        URL url = getClass().getResource("/protein/" + name + ".ent.gz");
        File file = new File(url.getFile());
        return file;
    }

    /**
     * Wrzuca pliki z folderu o właściwym rozszerzeniu do tablicy
     *
     * @param directory folder w którym szukamy plików
     * @param extension rozszerzenie plików
     * @throws IOException błąd przy korzystaniu z plikami
     */
    private void getContentOfFilesInDirectory(Query query, File directory, String extension) throws IOException {
        File[] currentFiles = directory.listFiles();
        for (File file : currentFiles) {
            if (file.isFile()
                    && file.getName().endsWith(extension)) {
                String fileName = file.getName()
                        .substring(0, file.getName().indexOf(extension));
                if (((query.getFrom() != null) ? (fileName.compareToIgnoreCase(query.getFrom()) >= 0) : true)
                        && ((query.getTo() != null) ? (fileName.compareToIgnoreCase(query.getTo()) <= 0) : true)) {
                    this.files.add(file);
                } else {
                    if (query.getNames() != null) {
                        for (String name : query.getNames()) {
                            if (name.equals(fileName)) {
                                this.files.add(file);
                            }
                        }
                    }
                }
            } else if (file.isDirectory() && recursive) {
                getContentOfFilesInDirectory(file, extension);
            }
        }
    }

    /**
     * Wrzuca pliki z folderu o właściwym rozszerzeniu do tablicy
     *
     * @param directory folder w którym szukamy plików
     * @param extension rozszerzenie plików
     * @throws IOException błąd przy korzystaniu z plikami
     */
    private void getContentOfFilesInDirectory(File directory, String extension) throws IOException {
        File[] currentFiles = directory.listFiles();
        for (File file : currentFiles) {
            if (file.isFile() || file.getName().endsWith(extension)) {
                this.files.add(file);
            } else if (file.isDirectory() && recursive) {
                getContentOfFilesInDirectory(file, extension);
            }
        }
    }

    public boolean isChain(String name, String chain) {
        if (chain == null || chain.isEmpty()) {
            return true;
        }
        PDBFileReader pdbreader = new PDBFileReader();
        try {
            Structure struc = pdbreader.getStructure(getFileFromResource(name));
            return struc.getChainByPDB(chain) != null;
        } catch (IOException | StructureException ex) {
            return false;
        }

    }
//    public Protein getPhosphorus(File file,String chain) {
//        PDBFileReader pdbreader = new PDBFileReader();
//	try{
//	    Structure struc = pdbreader.getStructure(file);
//            Atom[] ca = new Atom[0];
//            List<Atom> atoms = new ArrayList<>();
//            for(Group group:struc.getChainByPDB(chain).getAtomGroups()){
//                Atom atom = group.getAtom("P");
//                atoms.add(atom);
//            }
//            ca = atoms.toArray(ca);
//            Protein protein = new Protein(file.getName().replace(".ent.gz", ""),ca);
//            return protein;
//
//	 } catch (IOException | StructureException ex) {
//            Logger.getLogger(ResourceManager.class.getName()).log(Level.SEVERE, null, ex);
//            return new Protein(file.getName().replace(".ent.gz",""), null);
//        }
//    }

    /**
     * Zwraca białko zamieszczone w pliku z białkami
     *
     * @param file plik z białkiem
     * @param chain testowany blok
     * @return encje białka.g
     */
    public Protein parseFileToProtein(File file, String chain) {
        PDBFileReader pdbreader = new PDBFileReader();
        try {
            Structure struc = pdbreader.getStructure(file);
            Atom[] ca;
            if (chain == null || chain.isEmpty()) {
                ca = StructureTools.getAtomCAArray(struc);
            } else {
                ca = StructureTools.getAtomCAArray(struc.getChainByPDB(chain));
            }
            Protein protein = new Protein(file.getName().replace(".ent.gz", ""), ca);
            return protein;

        } catch (IOException | StructureException ex) {
            Logger.getLogger(ResourceManager.class.getName()).log(Level.SEVERE, null, ex);
            return new Protein(file.getName().replace(".ent.gz", ""), new Atom[0]);
        }
    }
}
