package pl.polsl.kamilzur.proteinmemoryengine.threads;

import java.io.File;
import java.util.List;

import org.biojava.nbio.structure.StructureException;

import pl.polsl.kamilzur.proteinmemoryengine.Config;
import pl.polsl.kamilzur.proteinmemoryengine.beans.CalculateProteinBean;
import pl.polsl.kamilzur.proteinmemoryengine.calculation.Calculation;
import pl.polsl.kamilzur.proteinmemoryengine.calculation.CalculationFactory;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;
import pl.polsl.kamilzur.proteinmemoryengine.entities.Query;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;
import pl.polsl.kamilzur.proteinmemoryengine.resource.ResourceManager;
import pl.polsl.kamilzur.proteinmemoryengine.storage.ProteinStorageFactory;

/**
 *
 * @author Kamil Żur
 */
public class CalculationThreat implements Runnable {

	private static final Object SYNCHRONIZED_OBJECT_COUNT = new Object();
	private static int count = 0;
	private final Query query;
	private final String calculateName;
	private int id;
	private static int nextId = 0;
	private final ResourceManager RESOURCE_MANAGER = new ResourceManager();

	public CalculationThreat(Query query, String calculateName) {
		this.query = query;
		this.calculateName = calculateName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static int runThread(Query query, String algoritmName) {
		Thread thread = null;
		int i = 0;
		while (i < 20) {
			try {
				CompareThreat compare = null;
				synchronized (SYNCHRONIZED_OBJECT_COUNT) {
					if (Config.getClientsInOneTime() != null ? count < Config.getClientsInOneTime() : true) {
						compare = new CompareThreat(query, algoritmName);
						compare.setId(query.getId() != null ? query.getId() : nextId++);
						thread = new Thread(compare);
						count++;
					}
				}
				if (compare != null && thread != null) {
					thread.start();
					return compare.getId();
				}
				Thread.sleep(1000);
			} catch (InterruptedException ex) {

			} finally {
				i++;
			}
		}
		return -1;
	}

	public String calculate(Query query, Integer id, Calculation calculation) {
		StringBuilder str = new StringBuilder();
		try {
			Protein proteinFirst;
			if (query.getProtein() != null) {
				proteinFirst = RESOURCE_MANAGER
						.parseFileToProtein(RESOURCE_MANAGER.getFileFromResource(query.getProtein()), query.getChain());
			} else {
				throw new InformationException("Brak białka do porównania");
			}
			ResourceManager rm = new ResourceManager();
			List<File> filesToCompare = rm.getAllFileFromResource(query, "/protein", ".ent.gz");
			List<String> namesFileToCompare = rm.getFilesName(filesToCompare, ".ent.gz");
			int i = 0;
			for (String nameFileToCompare : namesFileToCompare) {
				try {
					Protein proteinSecound = getProteinSecound(query, nameFileToCompare);
					if (proteinSecound != null) {
						String calculatedValue = calculation.run(proteinFirst, proteinSecound);
						StringBuilder xmlResult = new StringBuilder();
						xmlResult.append("<result>");
						xmlResult.append("<method>").append(calculation.getShortName()).append("</method>");
						xmlResult.append("<firstProtein>").append(proteinFirst.getName()).append("</firstProtein>");
						xmlResult.append("<secondProtein>").append(proteinFirst.getName()).append("</secondProtein>");
						xmlResult.append("<value>").append(calculatedValue).append("</value>");
						xmlResult.append("</result>");
						CalculateProteinBean.addResult(id, xmlResult.toString());
						str.append(xmlResult);
						i++;
						if (Config.getHowManyElementsWithOneQuery() != null
								? i >= Config.getHowManyElementsWithOneQuery() : false) {
							break;
						}
					}
				} catch (StructureException ex) {
					CalculateProteinBean.addResult(id, "<problem>Problem z plikiem</problem>");
				}
			}
		} catch (InformationException ex) {

		}
		return str.toString();
	}

	protected Protein getProteinSecound(Query query, String nameFileToCompare) {
		try {
			List<Protein> list = ProteinStorageFactory.getProteinStorage().getProteinList();
			for (Protein protein : list) {
				if (protein.getAtoms() != null && protein.getName().equals(nameFileToCompare)) {
					return protein;
				}
			}
			return getSecoundProteinFromResource(nameFileToCompare);
		} catch (InformationException ex) {

		}
		return null;
	}

	protected Protein getSecoundProteinFromResource(String secoundProteinName) {
		ResourceManager rm = new ResourceManager();
		return rm.parseFileToProtein(rm.getFileFromResource(secoundProteinName), "");
	}

	@Override
	public void run() {
		Calculation calculation = CalculationFactory.getAlgorithm(calculateName);
		calculate(query, query.getId(), calculation);
		synchronized (SYNCHRONIZED_OBJECT_COUNT) {
			count--;
		}
	}
}
