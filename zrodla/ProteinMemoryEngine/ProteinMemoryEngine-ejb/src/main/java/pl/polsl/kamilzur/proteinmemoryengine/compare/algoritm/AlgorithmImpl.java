package pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm;

import org.biojava.nbio.structure.StructureException;
import org.biojava.nbio.structure.align.StructureAlignment;
import org.biojava.nbio.structure.align.ce.ConfigStrucAligParams;
import org.biojava.nbio.structure.align.model.AFPChain;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;

/**
 *
 * @author Kamil Żur
 */
public abstract class AlgorithmImpl implements Algorithm {

    private final ConfigStrucAligParams PARAMS;
    private final StructureAlignment ALGORITHM;

    public AlgorithmImpl(ConfigStrucAligParams params, StructureAlignment algorithm) {
        PARAMS = params;
        ALGORITHM = algorithm;
    }

    @Override
    public AFPChain compare(Protein proteinFirst, Protein proteinSecound) throws StructureException {
        AFPChain afpChain = ALGORITHM.align(proteinFirst.getAtoms(), proteinSecound.getAtoms(), PARAMS);
        afpChain.setName1(proteinFirst.getName());
        afpChain.setName2(proteinSecound.getName());
        return afpChain;
    }
}
