package pl.polsl.kamilzur.proteinmemoryengine.threads;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.biojava.nbio.structure.StructureException;
import org.biojava.nbio.structure.align.model.AFPChain;
import org.biojava.nbio.structure.align.model.AfpChainWriter;
import org.biojava.nbio.structure.align.xml.AFPChainXMLConverter;

import pl.polsl.kamilzur.proteinmemoryengine.Config;
import pl.polsl.kamilzur.proteinmemoryengine.beans.ComparingProteinsBean;
import pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm.Algorithm;
import pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm.AlgorithmFactory;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;
import pl.polsl.kamilzur.proteinmemoryengine.entities.Query;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;
import pl.polsl.kamilzur.proteinmemoryengine.resource.ResourceManager;
import pl.polsl.kamilzur.proteinmemoryengine.storage.ProteinStorageFactory;

/**
 *
 * @author Kamil Żur
 */
public class CompareThreat implements Runnable {

    private static final Object SYNCHRONIZED_OBJECT_COUNT = new Object();
    private static int count = 0;
    private final Query query;
    private final String algoritmName;
    private int id;
    private static int nextId = 0;
    private final ResourceManager RESOURCE_MANAGER = new ResourceManager();

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public CompareThreat(Query query, String algoritmName) {
        this.query = query;
        this.algoritmName = algoritmName;
    }

    public static int runThread(Query query, String algoritmName) {
        Thread thread = null;
        int i = 0;
        while (i < 20) {
            try {
                CompareThreat compare = null;
                synchronized (SYNCHRONIZED_OBJECT_COUNT) {
                    if (Config.getClientsInOneTime() != null ? count < Config.getClientsInOneTime() : true) {
                        compare = new CompareThreat(query, algoritmName);
                        compare.setId(query.getId() != null ? query.getId() : nextId++);
                        thread = new Thread(compare);
                        count++;
                    }
                }
                if (compare != null && thread != null) {
                    thread.start();
                    return compare.getId();
                }
                Thread.sleep(1000);
            } catch (InterruptedException ex) {

            } finally {
                i++;
            }
        }
        return -1;
    }

    public void compare(Query query, Integer id, Algorithm algorithm) {
        try {
            Protein proteinFirst;
            if (query.getProtein() != null) {
                proteinFirst = RESOURCE_MANAGER.parseFileToProtein(RESOURCE_MANAGER.getFileFromResource(query.getProtein()), query.getChain());
            } else {
                throw new InformationException("Brak białka do porównania");
            }
            ResourceManager rm = new ResourceManager();
            List<File> filesToCompare = rm.getAllFileFromResource(query, "/protein", ".ent.gz");
            List<String> namesFileToCompare = rm.getFilesName(filesToCompare, ".ent.gz");
            int i = 0;
            for (String nameFileToCompare : namesFileToCompare) {
                Protein proteinSecound = getProteinSecond(query, nameFileToCompare);
                AFPChain afpChain = algorithm.compare(proteinFirst, proteinSecound);
                afpChain.setName1(proteinFirst.getName());
                afpChain.setName2(proteinSecound.getName());
                ComparingProteinsBean.addResult(id, AFPChainXMLConverter.toXML(
                        afpChain, proteinFirst.getAtoms(), proteinSecound.getAtoms()), AfpChainWriter.toWebSiteDisplay(
                        afpChain, proteinFirst.getAtoms(), proteinSecound.getAtoms()));
                AFPChainXMLConverter.toXML(afpChain, proteinFirst.getAtoms(), proteinSecound.getAtoms());
                i++;
                if (Config.getHowManyElementsWithOneQuery() != null ? i >= Config.getHowManyElementsWithOneQuery() : false) {
                    break;
                }
            }
        } catch (InformationException ex) {

        } catch (StructureException | IOException ex) {
            ComparingProteinsBean.addResult(id, "<problem>Problem z plikiem</problem>", null);
        }
    }

    protected Protein getProteinSecond(Query query, String nameFileToCompare) {
        try {
            List<Protein> list = ProteinStorageFactory.getProteinStorage().getProteinList();
            for (Protein protein : list) {
                if (protein.getAtoms() != null
                        && protein.getName().equals(nameFileToCompare)) {
                    return protein;
                }
            }
            return getSecondProteinFromResource(nameFileToCompare);
        } catch (InformationException ex) {

        }
        return null;
    }

    protected Protein getSecondProteinFromResource(String secondProteinName) {
        ResourceManager rm = new ResourceManager();
        return rm.parseFileToProtein(rm.getFileFromResource(secondProteinName), "");
    }

    @Override
    public void run() {
        Algorithm algorithm = AlgorithmFactory.getAlgorithm(algoritmName);
        compare(query, query.getId(), algorithm);
        synchronized (SYNCHRONIZED_OBJECT_COUNT) {
            count--;
        }
    }
}
