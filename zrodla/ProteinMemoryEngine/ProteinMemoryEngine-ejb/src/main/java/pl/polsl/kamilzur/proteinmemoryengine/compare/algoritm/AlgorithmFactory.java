package pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm;

import org.biojava.nbio.structure.StructureException;

/**
 *
 * @author Kamil Żur
 */
public class AlgorithmFactory {

    public static Algorithm getAlgorithm(String algorithmName) {
        try {
            if (CeAlgorithm.ALGORITHM_NAME.equals(algorithmName)) {
                return CeAlgorithm.getInstance();
            }
            if (CeCPAlgoritm.ALGORITHM_NAME.equals(algorithmName)) {
                return CeCPAlgoritm.getInstance();
            }
            if (FatCatFlexibleAlgorithm.ALGORITHM_NAME.equals(algorithmName)) {
                return FatCatFlexibleAlgorithm.getInstance();
            }
            if (FatCatRigidAlgorithm.ALGORITHM_NAME.equals(algorithmName)) {
                return FatCatRigidAlgorithm.getInstance();
            }

            return null;
        } catch (StructureException ex) {
            return null;
        }
    }
;
}
