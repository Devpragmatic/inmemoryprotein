package pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm;

import org.biojava.nbio.structure.StructureException;
import org.biojava.nbio.structure.align.StructureAlignmentFactory;
import org.biojava.nbio.structure.align.fatcat.FatCatRigid;
import org.biojava.nbio.structure.align.fatcat.calc.FatCatParameters;

/**
 *
 * @author Kamil Żur
 */
public class FatCatRigidAlgorithm extends AlgorithmImpl {

    public static final String ALGORITHM_NAME = FatCatRigid.algorithmName;

    public static FatCatRigidAlgorithm getInstance() throws StructureException {
        return new FatCatRigidAlgorithm();
    }

    private FatCatRigidAlgorithm() throws StructureException {
        super(new FatCatParameters(), StructureAlignmentFactory.getAlgorithm(ALGORITHM_NAME));
    }
}
