package pl.polsl.kamilzur.proteinmemoryengine.calculation;

import org.biojava.nbio.structure.StructureException;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;

/**
 *
 * @author mon3k69
 */
public interface Calculation {

    public String run(Protein proteinFirst, Protein proteinSecound) throws StructureException;

    public Object getShortName();

}
