/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.kamilzur.proteinmemoryengine.beans;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Result;

/**
 *
 * @author Kamil Żur
 */
public class CalculateProteinBean {

    private static final ConcurrentHashMap<Integer, List<Result>> RESULTS = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<Integer, Date> TIMER = new ConcurrentHashMap<>();
    private static final Object OBJECT_SYNCHRONIZED_RESULTS = new Object();

    public static void addResult(Integer id, String result) {
        synchronized (OBJECT_SYNCHRONIZED_RESULTS) {
            if (RESULTS.get(id) == null) {
                RESULTS.put(id, new ArrayList<Result>());
                TIMER.put(id, new Date());
            }
            RESULTS.get(id).add(new Result(result, ""));
        }
    }

    public static void checkTimer() {
        synchronized (OBJECT_SYNCHRONIZED_RESULTS) {
            Enumeration<Integer> elements = TIMER.keys();
            List<Integer> toDelete = new ArrayList<>();
            while (elements.hasMoreElements()) {
                Integer id = elements.nextElement();
                Date date = TIMER.get(id);
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.DAY_OF_MONTH, 2);
                if (date.before(cal.getTime())) {
                    toDelete.add(id);
                }
            }
            for (Integer id : toDelete) {
                RESULTS.remove(id);
                TIMER.remove(id);
            }
        }
    }

    public String getResult(Integer id) {
        JSONObject result = new JSONObject();
        try {
            result.put("id", id);
            JSONArray values = new JSONArray();
            synchronized (OBJECT_SYNCHRONIZED_RESULTS) {
                if (RESULTS.get(id) != null) {
                    for (Result str : RESULTS.get(id)) {
                        try {
                            JSONObject json = XML.toJSONObject(str.getXml());
                            json.put("html", "<pre>" + str.getHtml() + "</pre>");
                            values.put(json);
                        } catch (JSONException ex) {
                            JSONObject json = new JSONObject();
                            json.put("problem", str + " " + ex.getMessage());
                            values.put(json);
                        }
                    }
                }
            }
            result.put("results", values);

        } catch (JSONException ex) {
            Logger.getLogger(ComparingProteinsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result.toString();
    }
}
