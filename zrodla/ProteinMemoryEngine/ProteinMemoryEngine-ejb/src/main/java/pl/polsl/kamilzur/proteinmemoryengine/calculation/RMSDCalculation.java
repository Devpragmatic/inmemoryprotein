package pl.polsl.kamilzur.proteinmemoryengine.calculation;

import org.biojava.nbio.structure.Atom;
import org.biojava.nbio.structure.StructureException;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;

/**
 *
 * @author Kamil Żur
 */
public class RMSDCalculation implements Calculation {

    private RMSDCalculation() {
    }
    public static final String ALGORITHM_NAME = "RMSD";

    @Override
    public String run(Protein proteinFirst, Protein proteinSecound) throws StructureException {
        Atom[] atoms1 = proteinFirst.getAtoms();
        Atom[] atoms2 = proteinSecound.getAtoms();
        if (atoms1.length != atoms2.length) {
            if (atoms1.length > atoms2.length) {
                Atom[] tmpAtoms = atoms1;
                atoms1 = atoms2;
                atoms2 = tmpAtoms;
            }
        }
        double rmsd = 0;
        for (int i = 0; i < atoms1.length; i++) {
            Atom atomLeft = atoms1[i];
            Atom atomRight = atoms2[i];
            double distance = calculateDistance(atomLeft, atomRight);
            rmsd += distance * distance;
        }
        rmsd /= atoms1.length;
        rmsd = Math.sqrt(rmsd);
        return String.valueOf(rmsd);
    }

    private double calculateDistance(Atom left, Atom right) {
        double dx = left.getX() - right.getX();
        double dy = left.getY() - right.getY();
        double dz = left.getZ() - right.getZ();
        return Math.sqrt(dx * dx + dy * dy + dz * dz);
    }

    @Override
    public Object getShortName() {
        return "RMSD";
    }

    public static RMSDCalculation getInstance() throws StructureException {
        return new RMSDCalculation();
    }
}
