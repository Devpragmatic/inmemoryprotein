package pl.polsl.kamilzur.proteinmemoryengine.entites;

/**
 *
 * @author Kamil Żur
 */
public class Result {

    private final String xml;
    private final String html;

    public Result(String xml, String html) {
        this.xml = xml;
        this.html = html;
    }

    public String getXml() {
        return xml;
    }

    public String getHtml() {
        return html;
    }

}
