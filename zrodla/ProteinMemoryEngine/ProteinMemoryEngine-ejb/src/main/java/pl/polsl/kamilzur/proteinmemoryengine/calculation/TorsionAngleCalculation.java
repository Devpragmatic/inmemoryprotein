package pl.polsl.kamilzur.proteinmemoryengine.calculation;

import org.biojava.nbio.structure.Atom;
import org.biojava.nbio.structure.Calc;
import org.biojava.nbio.structure.StructureException;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;

/**
 *
 * @author Kamil Żur
 */
public class TorsionAngleCalculation implements Calculation {

    private TorsionAngleCalculation() {
    }

    public static final String ALGORITHM_NAME = "TorsionAngle";

    @Override
    public String run(Protein proteinFirst, Protein proteinSecound) throws StructureException {
        StringBuilder result = new StringBuilder();
        Atom[] atoms = proteinFirst.getAtoms();
        for (int i = 0; i + 3 < atoms.length; i++) {
            double torsion = Calc.torsionAngle(atoms[i], atoms[i + 1], atoms[i + 2], atoms[i + 3]);
            result.append(torsion).append(",");
        }
        if (result.length() > 0) {
            result.deleteCharAt(result.lastIndexOf(","));
        }
        return result.toString();
    }

    @Override
    public Object getShortName() {
        return "TorsionAngle";
    }

    public static TorsionAngleCalculation getInstance() throws StructureException {
        return new TorsionAngleCalculation();
    }
}
