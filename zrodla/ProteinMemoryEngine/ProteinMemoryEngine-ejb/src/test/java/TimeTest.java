
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.time.StopWatch;
import org.biojava.nbio.structure.StructureException;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.polsl.kamilzur.proteinmemoryengine.Config;
import pl.polsl.kamilzur.proteinmemoryengine.calculation.RMSDCalculation;
import pl.polsl.kamilzur.proteinmemoryengine.calculation.TorsionAngleCalculation;
import pl.polsl.kamilzur.proteinmemoryengine.compare.algoritm.CeAlgorithm;
import pl.polsl.kamilzur.proteinmemoryengine.entites.Protein;
import pl.polsl.kamilzur.proteinmemoryengine.entities.Query;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;
import pl.polsl.kamilzur.proteinmemoryengine.resource.ResourceManager;
import pl.polsl.kamilzur.proteinmemoryengine.storage.ProteinStorageFactory;
import pl.polsl.kamilzur.proteinmemoryengine.threads.CalculationThreat;
import pl.polsl.kamilzur.proteinmemoryengine.threads.CompareThreat;
import pl.polsl.kamilzur.proteinmemoryengine.threads.FillProteinList;

/**
 *
 * @author Kamil Żur
 */
public class TimeTest {
	private class CompareInMemoryTest extends CompareThreat {

		public CompareInMemoryTest(Query query, String algoritmName) {
			super(query, algoritmName);
		}

		@Override
		protected Protein getProteinSecond(Query query, String nameFileToCompare) {
			try {
				List<Protein> list = ProteinStorageFactory.getProteinStorage().getProteinList();
				for (Protein protein : list) {
					if (protein.getAtoms() != null && protein.getName().equals(nameFileToCompare)) {
						return protein;
					}
				}
			} catch (InformationException ex) {
				Logger.getLogger(TimeTest.class.getName()).log(Level.SEVERE, null, ex);
			}
			return null;
		};

		public Protein get(String nameFileToCompare) {
			return getProteinSecond(null, nameFileToCompare);
		}

	}

	private class CompareWithoutMemoryTest extends CompareThreat {

		public CompareWithoutMemoryTest(Query query, String algoritmName) {
			super(query, algoritmName);
		}

		@Override
		protected Protein getProteinSecond(Query query, String nameFileToCompare) {
			return getSecondProteinFromResource(nameFileToCompare);
		};

		public Protein get(String nameFileToCompare) {
			return getProteinSecond(null, nameFileToCompare);
		}

	}

	private class CalculateInMemoryTest extends CalculationThreat {

		public CalculateInMemoryTest(Query query, String algoritmName) {
			super(query, algoritmName);
		}

		@Override
		protected Protein getProteinSecound(Query query, String nameFileToCompare) {
			try {
				List<Protein> list = ProteinStorageFactory.getProteinStorage().getProteinList();
				for (Protein protein : list) {
					if (protein.getAtoms() != null && protein.getName().equals(nameFileToCompare)) {
						return protein;
					}
				}
			} catch (InformationException ex) {
				Logger.getLogger(TimeTest.class.getName()).log(Level.SEVERE, null, ex);
			}
			return null;
		}
	}

	private class CalculateWithoutMemoryTest extends CalculationThreat {

		public CalculateWithoutMemoryTest(Query query, String algoritmName) {
			super(query, algoritmName);
		}

		@Override
		protected Protein getProteinSecound(Query query, String nameFileToCompare) {
			return getSecoundProteinFromResource(nameFileToCompare);
		}

	}

	private static class TestConfiguration {
		private static int ELEMENTS_IN_LIST = 1;
	}

	@BeforeClass
	public static void setUpClass() throws InformationException {
		System.out.println("StartClass");
		Config.setHowManyElementsWithOneQuery(TestConfiguration.ELEMENTS_IN_LIST);
		ProteinStorageFactory.getProteinStorage().clear();
		ResourceManager rm = new ResourceManager();
		List<File> filesToCompare = rm.getAllFileFromResource(new Query(null, null, null, null, null, null),
				"/protein", ".ent.gz");
		List<String> namesFileToCompare = rm.getFilesName(filesToCompare, ".ent.gz");
		Integer sizeOfList = TestConfiguration.ELEMENTS_IN_LIST;
		if (sizeOfList > namesFileToCompare.size()) {
			sizeOfList = namesFileToCompare.size();
		} else if (sizeOfList < namesFileToCompare.size()) {
			int random = new Random().nextInt(namesFileToCompare.size() - sizeOfList);
			namesFileToCompare = namesFileToCompare.subList(random, sizeOfList + random);
		}
		FillProteinList.runThread(new Query(namesFileToCompare, "", "", null, null, null));
		while (ProteinStorageFactory.getProteinStorage().getProteinList().size() < sizeOfList)
			;
		System.out.println("End StartClass");
	}

	@Test
	public void comparing() throws InformationException, StructureException {
		System.out.println("Start CompareTest");
		List<String> namesFileToCompare = new ArrayList<>();
		for (Protein protein : ProteinStorageFactory.getProteinStorage().getProteinList()) {
			namesFileToCompare.add(protein.getName());
		}
		StopWatch sw1 = new StopWatch();
		CompareThreat ct = new CompareInMemoryTest(new Query(namesFileToCompare, null, null, null, null, null),
				CeAlgorithm.ALGORITHM_NAME);
		System.out.println("Start Memory timeTest");
		sw1.start();
		if (namesFileToCompare.size() > 0)
			ct.compare(new Query(namesFileToCompare, "", "", "pdb8ico", null, null), 0, CeAlgorithm.getInstance());
		sw1.stop();
		System.out.println("Stop Memory timeTest");
		System.out.println(
				"Memory time: " + sw1.getTime() + " milliseconds for " + namesFileToCompare.size() + " proteins");
		sw1.reset();
		ct = new CompareWithoutMemoryTest(new Query(namesFileToCompare, null, null, null, null, null),
				CeAlgorithm.ALGORITHM_NAME);
		System.out.println("Start WithoutMemory timeTest");
		sw1.start();
		if (namesFileToCompare.size() > 0) {
			ct.compare(new Query(namesFileToCompare, "", "", "pdb8ico", null, null), 0, CeAlgorithm.getInstance());
		}
		sw1.stop();
		System.out.println("Stop WithoutMemory timeTest");
		System.out.println("WithoutMemory time: " + sw1.getTime() + " milliseconds for " + namesFileToCompare.size()
				+ " proteins");

		System.out.println("End CompareTest");
	}

	@Test
	public void getting() throws InformationException, StructureException {
		System.out.println("Start GetTest");
		List<String> namesFileToCompare = new ArrayList<>();
		for (Protein protein : ProteinStorageFactory.getProteinStorage().getProteinList()) {
			namesFileToCompare.add(protein.getName());
		}
		StopWatch sw1 = new StopWatch();
		CompareInMemoryTest ct = new CompareInMemoryTest(new Query(namesFileToCompare, null, null, null, null, null),
				CeAlgorithm.ALGORITHM_NAME);
		System.out.println("Start Memory timeTest");
		sw1.start();
		for (String str : namesFileToCompare)
			ct.get(str);
		sw1.stop();
		System.out.println("Stop Memory timeTest");
		System.out.println(
				"Memory time: " + sw1.getTime() + " milliseconds for " + namesFileToCompare.size() + " proteins");
		sw1.reset();
		CompareWithoutMemoryTest cts = new CompareWithoutMemoryTest(
				new Query(namesFileToCompare, null, null, null, null, null), CeAlgorithm.ALGORITHM_NAME);
		System.out.println("Start WithoutMemory timeTest");
		sw1.start();
		for (String str : namesFileToCompare)
			cts.get(str);
		sw1.stop();
		System.out.println("Stop WithoutMemory timeTest");
		System.out.println("WithoutMemory time: " + sw1.getTime() + " milliseconds for " + namesFileToCompare.size()
				+ " proteins");
		System.out.println("End GetTest");
	}

	@Test
	public void calculateTest() throws InformationException, StructureException {
		System.out.println("Start calculateTest");
		List<String> namesFileToCompare = new ArrayList<>();
		for (Protein protein : ProteinStorageFactory.getProteinStorage().getProteinList()) {
			namesFileToCompare.add(protein.getName());
		}
		StopWatch sw1 = new StopWatch();
		CalculateInMemoryTest ct = new CalculateInMemoryTest(
				new Query(namesFileToCompare, null, null, null, null, null), TorsionAngleCalculation.ALGORITHM_NAME);
		System.out.println("Start Memory timeTest");
		sw1.start();
		ct.calculate(new Query(namesFileToCompare, "", "", "pdb8ico", null, null), 0,
				TorsionAngleCalculation.getInstance());
		sw1.stop();
		System.out.println("Stop Memory timeTest");
		System.out.println(
				"Memory time: " + sw1.getTime() + " milliseconds for " + namesFileToCompare.size() + " proteins");
		sw1.reset();
		CalculateWithoutMemoryTest cts = new CalculateWithoutMemoryTest(
				new Query(namesFileToCompare, null, null, null, null, null), TorsionAngleCalculation.ALGORITHM_NAME);
		System.out.println("Start WithoutMemory timeTest");
		sw1.start();
		cts.calculate(new Query(namesFileToCompare, "", "", "pdb8ico", null, null), 0,
				TorsionAngleCalculation.getInstance());
		sw1.stop();
		System.out.println("Stop WithoutMemory timeTest");
		System.out.println("WithoutMemory time: " + sw1.getTime() + " milliseconds for " + namesFileToCompare.size()
				+ " proteins");
		System.out.println("End GetTest");
	}

	@Test
	public void calculateRMSDTest() throws InformationException, StructureException {
		System.out.println("Start calculateTest");
		List<String> namesFileToCompare = new ArrayList<>();
		for (Protein protein : ProteinStorageFactory.getProteinStorage().getProteinList()) {
			namesFileToCompare.add(protein.getName());
		}
		StopWatch sw1 = new StopWatch();
		CalculateInMemoryTest ct = new CalculateInMemoryTest(
				new Query(namesFileToCompare, null, null, null, null, null), RMSDCalculation.ALGORITHM_NAME);
		System.out.println("Start Memory timeTest");
		sw1.start();
		ct.calculate(new Query(namesFileToCompare, "", "", "pdb4raw", null, null), 0, RMSDCalculation.getInstance());
		sw1.stop();
		System.out.println("Stop Memory timeTest");
		System.out.println(
				"Memory time: " + sw1.getTime() + " for milliseconds " + namesFileToCompare.size() + " proteins");
		sw1.reset();
		CalculateWithoutMemoryTest cts = new CalculateWithoutMemoryTest(
				new Query(namesFileToCompare, null, null, null, null, null), RMSDCalculation.ALGORITHM_NAME);
		System.out.println("Start WithoutMemory timeTest");
		sw1.start();
		cts.calculate(new Query(namesFileToCompare, "", "", "pdb4raw", null, null), 0, RMSDCalculation.getInstance());
		sw1.stop();
		System.out.println("Stop WithoutMemory timeTest");
		System.out.println("WithoutMemory time: " + sw1.getTime() + " milliseconds for " + namesFileToCompare.size()
				+ " proteins");
		System.out.println("End GetTest");
	}
}
