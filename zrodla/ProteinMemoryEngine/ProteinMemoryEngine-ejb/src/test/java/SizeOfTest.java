import java.io.File;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import pl.polsl.kamilzur.proteinmemoryengine.Config;
import pl.polsl.kamilzur.proteinmemoryengine.entities.Query;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;
import pl.polsl.kamilzur.proteinmemoryengine.resource.ResourceManager;
import pl.polsl.kamilzur.proteinmemoryengine.storage.ProteinStorageFactory;
import pl.polsl.kamilzur.proteinmemoryengine.threads.FillProteinList;

/**
 *
 * @author Kamil Żur
 */
public class SizeOfTest {

	private final ResourceManager rm = new ResourceManager();

	private static class TestConfiguration {
		private static int ELEMENTS_IN_LIST = 5;
	}

	private List<String> namesFileToCompare;

	@Before
	public void setUp() throws InformationException {
		Config.setHowManyElementsWithOneQuery(TestConfiguration.ELEMENTS_IN_LIST);
		ProteinStorageFactory.getProteinStorage().clear();
		List<File> filesToCompare = rm.getAllFileFromResource(new Query(null, null, null, null, null, Integer.SIZE),
				"/protein", ".ent.gz");
		namesFileToCompare = rm.getFilesName(filesToCompare, ".ent.gz");
		Integer sizeOfList = TestConfiguration.ELEMENTS_IN_LIST;
		if (sizeOfList > namesFileToCompare.size()) {
			sizeOfList = namesFileToCompare.size();
		} else if (sizeOfList < namesFileToCompare.size()) {
			int random = new Random().nextInt(namesFileToCompare.size() - sizeOfList);
			namesFileToCompare = namesFileToCompare.subList(random, sizeOfList + random);
		}
	}

	@Test
	public void sizeList() throws InformationException {
		testMemorySize();
		testFilesSize();
	}

	private void testMemorySize() throws InformationException {
		final Runtime runtime = Runtime.getRuntime();
		Thread.yield();
		long used1 = used(runtime);
		try {
			FillProteinList.runThread(new Query(namesFileToCompare, "", "", null, null, null));
		} catch (Exception ex) {
			System.out.println(String.format("Problem with file %s", ex.getMessage()));
		}
		while (ProteinStorageFactory.getProteinStorage().getProteinList().size() != namesFileToCompare.size())
			;
		long used2 = used(runtime);
		double result = used2 - used1;
		if (result < 0) {
			// GC was performed.
			throw new RuntimeException("The eden space is not large enough to hold all the objects.");
		} else if (result == 0) {
			throw new RuntimeException(
					"Object is not large enough to register, try turning off the TLAB with -XX:-UseTLAB");
		}
		System.out.println("Size of list with random "
				+ ProteinStorageFactory.getProteinStorage().getProteinList().size() + " is " + result);
	}

	private long used(final Runtime runtime) {
		return runtime.totalMemory() - runtime.freeMemory();
	}

	private void testFilesSize() throws InformationException {
		Thread.yield();
		long used = 0;
		List<File> files = rm.getAllFileFromResource(new Query(namesFileToCompare, "", "", null, null, -1), "/protein",
				".ent.gz");
		for (File file : files) {
			used += file.length();
		}
		System.out.println("Size of files with random " + files.size() + " is " + used);
	}
}
