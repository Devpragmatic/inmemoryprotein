package pl.polsl.kamilzur.proteinmemoryengine.rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import pl.polsl.kamilzur.proteinmemoryengine.entities.ServerParameter;
import pl.polsl.kamilzur.proteinmemoryengine.remotebeaninterfaces.ManagerServerRemote;

/**
 *
 * @author Kamil Żur
 */
@Stateless
@Path("/manageserver")
public class ManagerServerRest {

    @EJB
    private ManagerServerRemote bean;

    @POST
    @Consumes({"application/json"})
    @Path("/startServer")
    public String startServer(ServerParameter parameters) {
        String result = bean.startServer(parameters.getClientsInOneTime(),
                parameters.getHowManyElementsWithOneQuery(),
                parameters.getFromInAllList(),
                parameters.getToInAllList(),
                parameters.getCustomFiles());

        return result;
    }

    @GET
    @Path("/getFilesName")
    public String fileInServer() {
        return bean.getFilesInServer();
    }

}
