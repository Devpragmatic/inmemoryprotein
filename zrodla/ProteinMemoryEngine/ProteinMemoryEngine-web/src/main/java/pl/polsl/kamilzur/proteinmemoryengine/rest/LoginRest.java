/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.kamilzur.proteinmemoryengine.rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import pl.polsl.kamilzur.proteinmemoryengine.entities.User;
import pl.polsl.kamilzur.proteinmemoryengine.remotebeaninterfaces.LoginRemote;

/**
 *
 * @author Kamil Żur
 */
@Stateless
@Path("/login")
public class LoginRest {

    @EJB
    private LoginRemote bean;

    @POST
    @Consumes({"application/json"})
    @Path("/user")
    public String startCE(User user) {
        return bean.login(user.getLogin(), user.getPassword()) ? "{\"result\": \"true\"}" : "{\"result\": \"false\"}";
    }
}
