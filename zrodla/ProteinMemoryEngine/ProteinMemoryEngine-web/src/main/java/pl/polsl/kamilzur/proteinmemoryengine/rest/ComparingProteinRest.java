package pl.polsl.kamilzur.proteinmemoryengine.rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import pl.polsl.kamilzur.proteinmemoryengine.entities.Query;
import pl.polsl.kamilzur.proteinmemoryengine.entities.ServerResult;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;
import pl.polsl.kamilzur.proteinmemoryengine.remotebeaninterfaces.ComparingProteinsRemote;

/**
 *
 * @author Kamil Żur
 */
@Stateless
@Path("/comparing")
public class ComparingProteinRest {

    @EJB
    private ComparingProteinsRemote bean;

    @POST
    @Consumes({"application/json"})
    @Path("/algorithmCE")
    public ServerResult startCE(Query query) {
        ServerResult result = new ServerResult();
        try {
            result.setId(bean.comparingProteinCE(query.getFrom(), query.getTo(), query.getNames(), query.getChain(), query.getChain(), query.getId()));
            return result;
        } catch (InformationException ex) {
            result.setId(-1);
            result.setResult(ex.getMessage());
            return result;
        }
    }

    @POST
    @Consumes({"application/json"})
    @Path("/algorithmCeCP")
    public ServerResult startCeCP(Query query) {
        ServerResult result = new ServerResult();
        try {
            result.setId(bean.comparingProteinCeCP(query.getFrom(), query.getTo(), query.getNames(), query.getProtein(), query.getChain(), query.getId()));
            return result;
        } catch (InformationException ex) {
            result.setId(-1);
            result.setResult(ex.getMessage());
            return result;
        }
    }

    @POST
    @Consumes({"application/json"})
    @Path("/algorithmFatCatFlexible")
    public ServerResult startFatCatFlexible(Query query) {
        ServerResult result = new ServerResult();
        try {
            result.setId(bean.comparingProteinFatCatFlexible(query.getFrom(), query.getTo(), query.getNames(), query.getProtein(), query.getChain(), query.getId()));
            return result;
        } catch (InformationException ex) {
            result.setId(-1);
            result.setResult(ex.getMessage());
            return result;
        }
    }

    @POST
    @Consumes({"application/json"})
    @Path("/algorithmFatCatRigid")
    public ServerResult startFatCatRigid(Query query) {
        ServerResult result = new ServerResult();
        try {
            result.setId(bean.comparingProteinFatCatRigid(query.getFrom(), query.getTo(), query.getNames(), query.getProtein(), query.getChain(), query.getId()));
            return result;
        } catch (InformationException ex) {
            result.setId(-1);
            result.setResult(ex.getMessage());
            return result;
        }
    }

    @POST
    @Consumes({"application/json"})
    @Path("/getResult")
    public String getCeResult(Query query) {
        return bean.getResultCE(query.getId());
    }
}
