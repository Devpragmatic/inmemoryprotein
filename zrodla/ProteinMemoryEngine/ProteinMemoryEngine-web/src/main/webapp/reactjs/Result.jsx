var Result = React.createClass({
    render: function () {
        return (
                < div>
                    <a className="btn btn-default" href="#" onClick={this.refresh}>{"Refresh"}</a>
                    <table ref="table" id="table" className="table table-striped table-bordered" cellspacing="0" width="100%">
                        <tbody>

                        </tbody>
                    </table>
                </div>
                )
    },
    table: null,
    componentDidMount : function(){
        this.table = $(this.refs.table.getDOMNode()).DataTable( {
                    data: [],
                    "scrollY": '40vh',
                    "scrollX": true,
                    columns: [
                       {
                            "className":      'details-control',
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": '',
                            title : "html"
                        },
                      { data: 'AFPChain.name1' ,title: 'First protein'},
                      { data: 'AFPChain.name2' ,title: 'Secound protein'},
                      { data: 'AFPChain.method' ,title: 'Method'},
                      { data: 'AFPChain.identity',title: 'Identity' },
                      { data: 'AFPChain.afpNum' ,title: 'afpNum'},
                      { data: 'AFPChain.ca1Length' ,title: 'calLength'},
                      { data: 'AFPChain.totalLenIni' ,title: 'totalLenIni'},
                      { data: 'AFPChain.alignScoreUpdate' ,title: 'alignScoreUpdate'},
                      { data: 'AFPChain.totalRmsdOpt' ,title: 'totalRmsdOpt'},
                      { data: 'AFPChain.normAlignScore' ,title: 'normAlignScore'},
                      { data: 'AFPChain.ca2Length' ,title: 'ca2Length'},
                      { data: 'AFPChain.similarity1' ,title: 'similarity1'},
                      { data: 'AFPChain.similarity2' ,title: 'similarity2'},
                      { data: 'AFPChain.version' ,title: 'version'},
                      { data: 'AFPChain.chainRmsd' ,title: 'chainRmsd'},
                      { data: 'AFPChain.time' ,title: 'time'},
                      { data: 'AFPChain.blockNum' ,title: 'blockNum'},
                      { data: 'AFPChain.similarity' ,title: 'similarity'},
                      { data: 'AFPChain.alnLength' ,title: 'alnLength'},
                      { data: 'AFPChain.probability' ,title: 'probability'},
                      { data: 'AFPChain.totalRmsdIni' ,title: 'totalRmsdIni'},
                      { data: 'AFPChain.optLength' ,title: 'optLength'},
                      { data: 'AFPChain.gapLen' ,title: 'gapLen'},
                      { data: 'AFPChain.alignScore' ,title: 'alignScore'}
                    ]
                });  
            var that = this;
            $('#table tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = that.table.row( tr );

                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    row.child( that.format(row.data()) ).show();
                    tr.addClass('shown');
                }
            } );
    },
    format : function( d ) {
        // `d` is the original data object for the row
        return d.html.toString();
    },
    refresh: function(){
        var that = this;
        $.ajax({
            url: "./rest/comparing/getResult",
            type: "POST",
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            data:JSON.stringify({id: parseInt(that.props.id!=null?that.props.id:-1)}),
            success:function(result){
                that.table.rows().remove();
                that.table.rows.add(result.results).draw();
            }
        });
    },
    componentWillMount : function () {
       this.setState({result:new Array()});
    }
});
