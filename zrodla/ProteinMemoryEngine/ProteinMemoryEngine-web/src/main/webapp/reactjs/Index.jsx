var Index = React.createClass({
    componentWillMount : function(){
        var that = this;
        this.setState({files:new Array()});
        $.ajax({
            url: "./rest/manageserver/getFilesName",
            type: "GET",
            dataType: "json",
            success:function(result){
                that.setState({files:result.files});
            }
        });
    },
    render: function () {
        return (
                <div>
                    <BsMenu />
                    <BsQuery files={this.state.files}/>
                </div>
                )
    }
});
