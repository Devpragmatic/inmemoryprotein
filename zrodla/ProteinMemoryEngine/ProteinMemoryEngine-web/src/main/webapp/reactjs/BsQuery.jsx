var BsQuery = React.createClass({
    propTypes: {
        files : React.PropTypes.array
    },
    getDefaultProps : function(){
        files : new Array()
    },
    validate: function(){
        return true;
    },
    sendDataToServer: function(){
        var that = this;
        if(this.validate()){
             $.ajax({
                url: "./rest/comparing/algorithm"+that.refs.algorithm.getDOMNode().value,
                type: "POST",
                dataType: "json",
                contentType: "application/json;charset=utf-8",
                data:JSON.stringify(that.getValue()),
                success:function(result){
                    if(result.id==-1)
                        alert(result.result);
                    else{    
                        that.setState({id:result.id});
                        alert("Query is processed. Your id is "+result.id);
                    }
                }
            });
        }
    },
    getValue: function(){
        var result = new Object();
        result.names = this.refs.names.getValue();
        result.to = this.refs.to.getDOMNode().value;
        result.from = this.refs.from.getDOMNode().value;
        result.protein = this.refs.protein.getDOMNode().value;
        result.chain = this.refs.chain.getDOMNode().value;
        if(this.state.id!=null){
            result.id = this.state.id;
        }
        return result;
    },
    componentWillReceiveProps: function(nextProps){
        this.setState({files:nextProps.files});  
    },
    componentWillMount : function(){
        this.setState({files:this.props.files});
    },
    render: function () {
        var that =this;
        return (
                <div>
                    <div className="form-horizontal">
                        <div className="col-sm-8">
                            <div className="form-group">
                                <div className="col-sm-6">
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <label className="control-label" forHtml="protein">{"Base protein to compare:"}</label>
                                            <select className="form-control" ref="protein" id="protein" >
                                             {this.state.files.map(function(element){
                                                return <option key={element} value={element}>{element}</option> 
                                              })}
                                            </select>
                                        </div>
                                        <div className="col-sm-6">
                                        <label className="control-label" forHtml="protein">{"chain:"}</label>
                                            <input className="form-control" ref="chain" maxLength="256"/>
                                        </div>
                                    </div>
                                    <label className="control-label" forHtml="algorithm">{"Algorithm:"}</label>
                                    <select className="form-control" ref="algorithm" id="algorithm">
                                        <option key="CE" value={"CE"}>CE</option> 
                                        <option key="CeCP" value={"CeCP"}>CeCP</option> 
                                        <option key="FatCatFlexible" value={"FatCatFlexible"}>FatCatFlexible</option> 
                                        <option key="FatCatRigid" value={"FatCatRigid"}>FatCatRigid</option> 
                                    </select>
                                    <button type="button" className="btn btn-success" onClick={this.sendDataToServer}>{"Compare"}</button>
                                </div>
                                <div className="col-sm-6">
                                    <label>{"Range of protein"}</label>
                                    <div className="form-group">
                                        <label className="control-label col-sm-2" forHtml="from">{"from:"}</label>
                                        <div className="col-sm-10">
                                          <input className="form-control" ref="from" onBlur={this.validate} id="from"/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label col-sm-2" forHtml="to">{'to:'}</label>
                                        <div className="col-sm-10">
                                          <input className="form-control" ref="to" onBlur={this.validate} id="to"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <Result id={this.state.id}/>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <label className="control-label"  forHtml="names">{"Custom protein:"}</label>
                            <BsMultiSelect ref="names" size="10" id="names" leftList={this.state.files}/>
                        </div>
                    </div>
                </div>
                )
    }
});
