var BsMultiSelect = React.createClass({
    propTypes: {
        leftList: React.PropTypes.array,
        rightList: React.PropTypes.array,
        size: React.PropTypes.string
    },
    getDefaultProps: function() {
        return {
            leftList: new Array(),
            rightList: new Array(),
            size: "15"
        };
    },
    componentWillMount : function(){
        this.setState({leftList: this.props.leftList,
                        rightList: this.props.rightList});
    },
    componentWillReceiveProps: function(nextProps) {
        this.setState({leftList: nextProps.leftList,
                        rightList: nextProps.rightList});
      },
    getValue: function(){
        return this.state.rightList;
    },
    toLeft: function(){
        var tmp = $(this.refs.sel2.getDOMNode()).val();
        if(tmp==null)
            return;
        var leftList = new Array();
        this.state.leftList.forEach(function(element){
           leftList.push(element);
        });
        tmp.forEach(function(element){
           leftList.push(element);
        });
        var rightList = new Array();
        this.state.rightList.forEach(function(element){
            var flaga = false;
            tmp.forEach(function(object){
                if(element==object)
                    flaga = true;
            });
            if(!flaga){
                rightList.push(element);
            }
        });
                this.setState({leftList: leftList,
                        rightList: rightList});
    },
    toRight: function(){
        var tmp = $(this.refs.sel1.getDOMNode()).val();
        if(tmp==null)
            return;
        var rightList = new Array();
        this.state.rightList.forEach(function(element){
           rightList.push(element);
        });
        tmp.forEach(function(element){
           rightList.push(element);
        });

        var leftList = new Array();
        this.state.leftList.forEach(function(element){
            var flaga = false;
            tmp.forEach(function(object){
                if(element==object)
                    flaga = true;
            });
            if(!flaga){
                leftList.push(element);
            }
        });
                this.setState({leftList: leftList,
                        rightList: rightList});
    },
    render: function () {
        return (
                <div>
                    <label for="sel1">Proteins:</label>
                    <select ref="sel1" multiple  size={this.props.size} className="form-control" id="sel1">
                        {this.state.leftList.map(function(element){
                           return <option key={element} value={element}>{element}</option> 
                         })}
                    </select>
                        <div className="col-xs-6">
                            <a href="#" onClick={this.toRight} className="btn btn-default glyphicon glyphicon-arrow-down pull-right"></a>
                        </div>
                        <div className="col-xs-6">
                            <a href="#" onClick={this.toLeft} className="btn btn-default glyphicon glyphicon-arrow-up pull-left"></a>
                        </div>
                    <label for="sel2">Selected:</label>
                    <select ref="sel2" size={this.props.size} multiple className="form-control" id="sel2">
                         {this.state.rightList.map(function(element){
                           return <option key={element} value={element}>{element}</option> 
                         })}
                    </select>
                </div>
                )
    }
});
