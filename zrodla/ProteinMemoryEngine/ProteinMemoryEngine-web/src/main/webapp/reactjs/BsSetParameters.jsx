var BsSetParameters = React.createClass({
    propTypes: {
        files : React.PropTypes.array
    },
    getDefaultProps : function(){
        files : new Array()
    },
    validate: function(){
        if(!this.validateNumber(this.refs.clientsInOneTime.getDOMNode().value))
            this.refs.clientsDiv.getDOMNode().className = "form-group has-success";
        else
            this.refs.clientsDiv.getDOMNode().className = "form-group has-error";
        if(!this.validateNumber(this.refs.howManyElementsWithOneQuery.getDOMNode().value))
            this.refs.elementsDiv.getDOMNode().className = "form-group has-success";
        else
            this.refs.elementsDiv.getDOMNode().className = "form-group has-error";
        return true;
    },
    validateNumber: function (value){
    return(isNaN(value));
    },
    sendDataToServer: function(){
        var that = this;
        if(this.validate()){
            $.ajax({
                url: "./rest/manageserver/startServer",
                type: "POST",
                dataType: "json",
                contentType: "application/json;charset=utf-8",
                data:JSON.stringify(that.getValue()),
                success:function(){
                    alert("Parameters correctly send to server");
                    $('#myModal').modal('hide');
                }
            });
        }
    },
    getValue: function(){
        var result = new Object();
        result.customFiles = this.refs.customFiles.getValue();
        result.toInAllList = this.refs.toInAllList.getDOMNode().value;
        result.fromInAllList = this.refs.fromInAllList.getDOMNode().value;
        result.clientsInOneTime = parseInt(this.refs.clientsInOneTime.getDOMNode().value);
        result.howManyElementsWithOneQuery = parseInt(this.refs.howManyElementsWithOneQuery.getDOMNode().value);
        return result;
    },
    componentWillReceiveProps: function(nextProps){
        this.setState({files:nextProps.files});  
    },
    componentWillMount : function(){
        this.setState({files:this.props.files});
    },
    render: function () {
        var that =this;
        return (
                <div>
                    <div id="myModal" className="modal fade" role="dialog">
                      <div className="modal-dialog" width="80%">

                        <div className="modal-content">
                          <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                            <h4 className="modal-title">Set server parameters</h4>
                          </div>
                          <div className="modal-body">
                            <div className="form-horizontal">
                                <div ref="clientsDiv" className="form-group">
                                    <label className="control-label" forHtml="clients">{"Maximum number of clients:"}</label>
                                    <input maxLength="5" className="form-control" ref="clientsInOneTime" id="clients" placeholder="Write the number" onBlur={this.validate}/>
                                </div>
                                <div ref="elementsDiv" className="form-group">
                                    <label className="control-label" forHtml="proteinsNumber">{"Maximum number of proteins in single query:"}</label>
                                    <input maxLength="5" className="form-control" ref="howManyElementsWithOneQuery" onBlur={this.validate} id="proteinsNumber" placeholder="Write the number"/>
                                </div>
                                <label>{"Proteins in memory:"}</label>
                                <div className="form-group">
                                    <label className="control-label col-sm-2" forHtml="fromInAllList">{"from:"}</label>
                                    <div className="col-sm-10">
                                      <input className="form-control"maxLength="256" ref="fromInAllList" onBlur={this.validate} id="fromInAllList" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="control-label col-sm-2" forHtml="toInAllList">{'to:'}</label>
                                    <div className="col-sm-10">
                                      <input className="form-control" maxLength="256" ref="toInAllList" onBlur={this.validate} id="toInAllList" />
                                    </div>
                                </div>
                                <label className="control-label"  forHtml="customFiles">{"Custom files:"}</label>
                                <BsMultiSelect ref="customFiles" leftList={this.state.files}/>
                            </div>
                          </div>
                          <div className="modal-footer">
                                <button type="button" className="btn btn-success" onClick={this.sendDataToServer}>{"Save"}</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                )
    }
});
