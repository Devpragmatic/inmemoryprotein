var BsMenu = React.createClass({
    render: function () {
        return (
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                    
                        <div className="navbar-header">
                              <button type="button" 
                                    className="navbar-toggle collapsed" 
                                    data-toggle="collapse" 
                                    data-target="#menu-navbar-collapsed" 
                                    aria-expanded="false">
                                <span className="sr-only">Options</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                              </button>
                            <a className="navbar-brand" href="#">{"Protein in-Memory Engine"}</a>
                        </div>
                    </div>
                </nav>
                )
    }
});
