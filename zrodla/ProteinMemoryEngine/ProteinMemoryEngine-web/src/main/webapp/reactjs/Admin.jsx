var Admin = React.createClass({
    componentWillMount : function(){
        var that = this;
        this.setState({files:new Array(),login:false});
        $.ajax({
            url: "./rest/manageserver/getFilesName",
            type: "GET",
            dataType: "json",
            success:function(result){
                that.setState({files:result.files});
            }
        });
    },
    login: function(){
        var that = this;
        var object = new Object();
        object.login = this.refs.login.getDOMNode().value;
        object.password = this.refs.password.getDOMNode().value;
        $.ajax({
            url: "./rest/login/user",
            type: "POST",
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify(object),
            success:function(result){
                that.setState({login:result.result});
            }
        });
    },
    render: function () {
        return (
            <div>
            {
                this.state.login=="true"?
                <div>
                    <nav className="navbar navbar-inverse">
                        <div className="container-fluid">

                            <div className="navbar-header">
                                  <button type="button" 
                                        className="navbar-toggle collapsed" 
                                        data-toggle="collapse" 
                                        data-target="#menu-navbar-collapsed" 
                                        aria-expanded="false">
                                    <span className="sr-only">Options</span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                  </button>
                                <a className="navbar-brand" href="#">{"Protein in-Memory Engine Admin Panel"}</a>
                            </div>

                            <div className="collapse navbar-collapse" id="menu-navbar-collapsed">
                                <ul className="nav navbar-nav">
                                    <li className="">
                                            <a href="#" data-toggle="modal" data-target="#myModal">
                                            {"Set server's parameters"}
                                            </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <BsSetParameters files={this.state.files}/>
                </div>
                :
                <div>
                    <div ref="clientsDiv" className="form-group">
                        <label className="control-label" forHtml="login">{"Login:"}</label>
                        <input className="form-control" ref="login" id="login"/>
                    </div>
                    <div ref="elementsDiv" className="form-group">
                        <label className="control-label" forHtml="password">{"Password:"}</label>
                        <input type="password" className="form-control" id="password" ref="password" />
                    </div>
                    <button className="btn btn-success" onClick={this.login}>Login</button>
                </div>
            }
            </div>
                )
    }
});
