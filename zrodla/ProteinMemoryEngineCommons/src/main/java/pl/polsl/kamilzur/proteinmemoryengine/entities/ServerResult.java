package pl.polsl.kamilzur.proteinmemoryengine.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kamil Żur
 */
@XmlRootElement
public class ServerResult {

    @XmlElement
    private String result;
    @XmlElement
    private Integer id;

    public ServerResult(String result, Integer id) {
        this.result = result;
        this.id = id;
    }

    public ServerResult() {
    }

    @Override
    public String toString() {
        return getResult();
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

}
