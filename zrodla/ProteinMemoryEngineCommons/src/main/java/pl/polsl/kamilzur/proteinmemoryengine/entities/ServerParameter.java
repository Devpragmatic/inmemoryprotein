package pl.polsl.kamilzur.proteinmemoryengine.entities;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kamil Żur
 */
@XmlRootElement
public class ServerParameter {

    public ServerParameter() {

    }

    public ServerParameter(Integer clientsInOneTime,
            Integer howManyElementsWithOneQuery,
            String fromInAllList,
            String toInAllList,
            List<String> customFiles) {
        this.clientsInOneTime = clientsInOneTime;
        this.howManyElementsWithOneQuery = howManyElementsWithOneQuery;
        this.fromInAllList = fromInAllList;
        this.toInAllList = toInAllList;
        this.customFiles = customFiles;
    }

    @XmlElement
    private Integer clientsInOneTime;
    @XmlElement
    private Integer howManyElementsWithOneQuery;
    @XmlElement
    private String fromInAllList;
    @XmlElement
    private String toInAllList;
    @XmlElement
    private List<String> customFiles;

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder(" custom files: ");
        if (getCustomFiles() != null) {
            for (String customFile : getCustomFiles()) {
                strBuilder.append(customFile).append(" ,");
            }
        }
        return new StringBuilder(" clients: ").append(getClientsInOneTime())
                .append(" elements: ").append(getHowManyElementsWithOneQuery())
                .append(" from: ").append(getFromInAllList())
                .append(" to: ").append(getToInAllList())
                .append(strBuilder.toString()).toString();
    }

    /**
     * @return the clientsInOneTime
     */
    public Integer getClientsInOneTime() {
        return clientsInOneTime;
    }

    /**
     * @param clientsInOneTime the clientsInOneTime to set
     */
    public void setClientsInOneTime(Integer clientsInOneTime) {
        this.clientsInOneTime = clientsInOneTime;
    }

    /**
     * @return the howManyElementsWithOneQuery
     */
    public Integer getHowManyElementsWithOneQuery() {
        return howManyElementsWithOneQuery;
    }

    /**
     * @param howManyElementsWithOneQuery the howManyElementsWithOneQuery to set
     */
    public void setHowManyElementsWithOneQuery(Integer howManyElementsWithOneQuery) {
        this.howManyElementsWithOneQuery = howManyElementsWithOneQuery;
    }

    /**
     * @return the fromInAllList
     */
    public String getFromInAllList() {
        return fromInAllList;
    }

    /**
     * @param fromInAllList the fromInAllList to set
     */
    public void setFromInAllList(String fromInAllList) {
        this.fromInAllList = fromInAllList;
    }

    /**
     * @return the toInAllList
     */
    public String getToInAllList() {
        return toInAllList;
    }

    /**
     * @param toInAllList the toInAllList to set
     */
    public void setToInAllList(String toInAllList) {
        this.toInAllList = toInAllList;
    }

    /**
     * @return the customFiles
     */
    public List<String> getCustomFiles() {
        return customFiles;
    }

    /**
     * @param customFiles the customFiles to set
     */
    public void setCustomFiles(List<String> customFiles) {
        this.customFiles = customFiles;
    }

}
