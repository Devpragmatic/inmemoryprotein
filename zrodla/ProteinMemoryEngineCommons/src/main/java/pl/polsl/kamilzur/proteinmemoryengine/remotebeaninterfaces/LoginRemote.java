package pl.polsl.kamilzur.proteinmemoryengine.remotebeaninterfaces;

import javax.ejb.Remote;

/**
 *
 * @author Kamil Żur
 */
@Remote
public interface LoginRemote {

    public Boolean login(String login, String password);
}
