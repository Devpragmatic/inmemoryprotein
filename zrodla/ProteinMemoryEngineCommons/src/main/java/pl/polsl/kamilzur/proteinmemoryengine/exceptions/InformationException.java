package pl.polsl.kamilzur.proteinmemoryengine.exceptions;

/**
 *
 * @author Kamil Żur
 */
public class InformationException extends Exception {

    private static final long serialVersionUID = 1L;

    public InformationException(String message) {
        super(message);
    }

}
