package pl.polsl.kamilzur.proteinmemoryengine.entities;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kamil Żur
 */
@XmlRootElement
public class Query {

    @XmlElement
    private List<String> names;
    @XmlElement
    private String from;
    @XmlElement
    private String to;
    @XmlElement
    private String protein;
    @XmlElement
    private Integer id;
    @XmlElement
    private String chain;

    public Query() {
    }

    public Query(List<String> names, String from, String to, String protein, String chain, Integer id) {
        this.names = names;
        this.from = from;
        this.to = to;
        this.protein = protein;
        this.chain = chain;
        this.id = id;
    }

    /**
     * @return the names
     */
    public List<String> getNames() {
        return names;
    }

    /**
     * @param names the names to set
     */
    public void setNames(List<String> names) {
        this.names = names;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the protein
     */
    public String getProtein() {
        return protein;
    }

    /**
     * @param protein the protein to set
     */
    public void setProtein(String protein) {
        this.protein = protein;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the chain
     */
    public String getChain() {
        return chain;
    }

    /**
     * @param chain the chain to set
     */
    public void setChain(String chain) {
        this.chain = chain;
    }

}
