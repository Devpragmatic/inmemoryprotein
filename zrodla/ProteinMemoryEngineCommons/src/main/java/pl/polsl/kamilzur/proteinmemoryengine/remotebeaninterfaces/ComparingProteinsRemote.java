/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.kamilzur.proteinmemoryengine.remotebeaninterfaces;

import java.util.List;
import javax.ejb.Remote;
import pl.polsl.kamilzur.proteinmemoryengine.exceptions.InformationException;

/**
 *
 * @author Kamil Żur
 */
@Remote
public interface ComparingProteinsRemote {

    public Integer comparingProteinCE(String from,
            String to,
            List<String> names,
            String protein, String chain, Integer id) throws InformationException;

    public String getResultCE(Integer id);

    public Integer comparingProteinCeCP(String from,
            String to,
            List<String> names,
            String protein,
            String chain,
            Integer id) throws InformationException;

    public Integer comparingProteinFatCatFlexible(String from,
            String to,
            List<String> names,
            String protein,
            String chain,
            Integer id) throws InformationException;

    public Integer comparingProteinFatCatRigid(String from,
            String to,
            List<String> names,
            String protein,
            String chain,
            Integer id) throws InformationException;
}
