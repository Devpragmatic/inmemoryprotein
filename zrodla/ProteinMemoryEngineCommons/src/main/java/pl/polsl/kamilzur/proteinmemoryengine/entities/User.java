package pl.polsl.kamilzur.proteinmemoryengine.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kamil Żur
 */
@XmlRootElement
public class User {

    public User() {

    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @XmlElement
    private String login;
    @XmlElement
    private String password;

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
