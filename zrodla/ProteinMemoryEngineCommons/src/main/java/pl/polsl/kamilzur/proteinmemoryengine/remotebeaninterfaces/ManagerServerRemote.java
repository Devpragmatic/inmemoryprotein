package pl.polsl.kamilzur.proteinmemoryengine.remotebeaninterfaces;

import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Kamil Żur
 */
@Remote
public interface ManagerServerRemote {

    public String startServer(Integer clientsInOneTime,
            Integer howManyElementsWithOneQuery,
            String fromInAllList,
            String toInAllList,
            List<String> customFiles);

    public String getFilesInServer();

    public void startCleaner();

    public void stopServer();

}
